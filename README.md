**Configurazione del sito**

Ho utilizzato laravel, quindi per far partire il progetto bisogna fare i classici passaggi.
Lanciare poi php artisan db:seed --class=Category
per riempire il DB con delle categorie preimpostate

**Google vision**
Per privacy, ho deciso di non condividere le credentials di google.
Se si vuole testare l'API di Google, bisogna:

-  Creare un account Google vision
- Esportare i dati personali in json
- Mettere il file nel main root del progetto e rinominarlo con google_credentials.json


**Funzionamento chat**

Per testare la chat bisogna:

- Creare un utente e renderlo Revisore con il comando php artisan presto:MakeUserRevisor ed inserire l'email
- Creare un annuncio e revisionarlo, andando sulla navbar->Home Revisore ed accettando un annuncio
- Loggarsi con un account user, andare nell'annuncio->dettaglio annuncio-> contatta e scrivere il messaggio.
- Loggarsi con l'account proprietario dell'annuncio e andare in navbar->messaggi
   qui abbiamo l'indice di tutti i messaggi filtrati, per mostrare solo l'ultimo ricevuto da parte di ogni utente
- Cliccando poi su visualizza chat, si ha accesso alla chat vera e propria, che mostra tutti i messaggi che coinvolgono i due attori


Per problemi di tempo, il codice e la funzionalità della chat non sono perfette, ma ci sto lavorando.

cose da fare:

- Rendere la chat Asincrona, magari con Vue

- Aggiungere notifica dei messaggi non letti

- Inserire nell'index anche gli ultimi messaggi inviati, se sono i piu recenti


Ogni consiglio/bug report è ben accetto. Potete contattarmi all'email Luan.Kerimi@gmail.com
Grazie
