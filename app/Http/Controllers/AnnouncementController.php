<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Jobs\ResizeImage;

use App\Models\Announcement;
use Illuminate\Http\Request;
use App\Models\AnnouncementImage;
use App\Jobs\GoogleVisionLabelImage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\Jobs\GoogleVisionSafeSearchImage;
use App\Http\Requests\AnnouncementRequest;
use App\Jobs\GoogleVisionRemoveFaces;

class AnnouncementController extends Controller
{

    
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'show');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $uniqueSecret = $request->old(
            'uniqueSecret',
            base_convert(sha1(uniqid(mt_rand())), 16, 36)
        );


        return view('announcement.create', compact('uniqueSecret'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request)
    {

        $user = Auth::user();
        $announcement = $user->announcements()->create([
            'title' => $request->title,
            'body' => $request->body,
            'category_id' => $request->category,
            'price' => $request->price,
        ]);

        $uniqueSecret = $request->input('uniqueSecret');

       
        $images = session()->get("images.{$uniqueSecret}", []);
     
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);


    
        foreach ($images as $image) {

            $i = new AnnouncementImage();
            $fileName = basename($image);
            $newFileName = "public/announcements/{$announcement->id}/{$fileName}";
            Storage::move($image, $newFileName);

            $i->file = $newFileName;
            $i->announcement_id = $announcement->id;
            $i->save();

            GoogleVisionSafeSearchImage::withChain([
                new GoogleVisionLabelImage($i->id),
                new GoogleVisionRemoveFaces($i->id),
                
                new ResizeImage($newFileName,300,150),
                new ResizeImage($newFileName,200,250),
                new ResizeImage($newFileName,400,300),
                new ResizeImage($newFileName,300,300),
                

            ])->dispatch($i->id);

            // dispatch(new GoogleVisionSafeSearchImage($i->id));
         
        }
        File::deleteDirectory(storage_path("/app/public/temp/{$uniqueSecret}"));

        return redirect(route('home'))->with('message', "Bene "  . Auth::user()->name . ", il tuo annuncio è stato inserito");


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        $announcements = Announcement::where('is_accepted', true)
            ->where('category_id', $announcement['category_id'])
            ->orderBy('created_at', 'desc')
            ->take(4)
            ->get();

        return view('announcement.show', compact('announcement', 'announcements'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function edit(Announcement $announcement, Request $request)
    {
       
        return view('announcement.edit', compact('announcement'));
 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function update(AnnouncementRequest $request, Announcement $announcement)
    {
        $mod = 'modificato';

        $announcement->title = $request->title;
        $announcement->body = $request->body;
        $announcement->price = $request->price;
        $announcement->category->id = $request->category;
        $announcement->is_accepted = null;
        $announcement->save();




        return redirect(route('home'))->with('message', "Bene "  . Auth::user()->name . ", il tuo annuncio è stato modificato");


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Announcement  $announcement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Announcement $announcement)
    {
        $images = AnnouncementImage::where('announcement_id', $announcement->id)->get();

        foreach ($images as $image) {
            $image->delete();
        }

        $announcement->delete();

        return redirect(route('userAnnouncements'))->with('message', "Bene "  . Auth::user()->name . ", il tuo annuncio eliminato");



    }

    /* LOGICA UPLOAD IMAGES */

    public function uploadImages(Request $request)
    {

        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->file('file')->store("public/temp/{$uniqueSecret}");

        dispatch(new ResizeImage(
            $fileName,
            120,
            120
        ));


        session()->push("images.{$uniqueSecret}", $fileName);

        return response()->json(
            ['id' => $fileName]
        );
    }


    public function removeImages(Request $request)
    {
        $uniqueSecret = $request->input('uniqueSecret');
        $fileName = $request->input('id');
        session()->push("removedimages.{$uniqueSecret}", $fileName);
        Storage::delete($fileName);
        return response()->json('ok');
    }

    public function getImages(Request $request)
    {

        $uniqueSecret = $request->input('uniqueSecret');

        $images = session()->get("images.{$uniqueSecret}", []);
        $removedImages = session()->get("removedimages.{$uniqueSecret}", []);

        $images = array_diff($images, $removedImages);

        $data = [];

        foreach ($images as $image) {

            $data[] = [
                'id' => $image,
                'src' => AnnouncementImage::getUrlByFilePath($image, 120, 120)
            ];
        }

        return response()->json($data);
    }
}
