<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Message;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $thisUser= Auth::user();      
        $messages_send = Message::where('user_id', Auth::user()->id) ->get();    
        $messages_addr = Message::where('to_id', Auth::user()->id) ->get(); 
        $messages_tot=$messages_send ->merge($messages_addr)->groupBy('user_id');  
           
         return view('message.index', compact( 'messages_tot', 'thisUser',));   
}





    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function create(Request $request, $user_id)
    {
        
        $sender= Auth::user()->id;
        $addressee = $user_id;
                 
        return view('message.create', compact('sender', 'addressee', 'user_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $addressee )
    {
      
        $user = Auth::user();
        $message = $user->messages()->create([
            'message' => $request->message,
            'to_id' => $addressee,  
        ]);

        return redirect()->back()->with('message', "Bene "  . Auth::user()->name . ", il tuo messaggio è stato inviato");

    }
     public function store2(Request $request, $addressee )
    {
      
        $user = Auth::user();
        $message = $user->messages()->create([
            'message' => $request->message,
            'to_id' => $addressee,  
        ]);

        return redirect(route('home'))->with('message', "Bene "  . Auth::user()->name . ", il tuo messaggio è stato inviato");

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        //
    }

    public function chatDetail($user_id){

        $thisUser= Auth::user();   
        $user_name = $user_id;       
        $messages_send = Message::where('user_id', Auth::user()->id)->where('to_id', $user_id) ->get();    
        $messages_addr = Message::where('to_id', Auth::user()->id)->where('user_id', $user_id) ->get(); 
        $messages_tot=$messages_send ->merge($messages_addr)->sortByDesc('created_at')->take(10)->sortBy('created_at');   
        $sender= Auth::user()->id;         
        $addressee= $user_name;

    
        return view('message.chat', compact('messages_tot', 'user_id', 'sender', 'addressee', 'thisUser', 'user_name'));
    }

}
