<?php

namespace App\Models;

use App\Models\User;

use Laravel\Scout\Searchable;
use App\Models\AnnouncementImage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Announcement extends Model
{
    use HasFactory;
    use Searchable;


    protected $fillable = [
        'title', 'body', 'user_id', 'category_id', 'price',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    static public function ToBeRevisionedCount()
    {

        return Announcement::where('is_accepted', null)->count();
    }

    //funzione che invia un array dei campi in cui fare la ricerca

    public function toSearchableArray()
    {
        $array = [
            'id' => $this->id,   //obbligatorio
            'title' => $this->title,
            'body' => $this->body,

        ];
        return $array;
    }

    public function images(){
        return $this->hasMany(AnnouncementImage::class);
        
    }

}
