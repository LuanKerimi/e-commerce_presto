<x-layout>
    <x-addannouncement/>
    <div class="container mt-5">
      <div class="row mt-5">
        <div class="col-12 col-md-8 offset-md-2 my-5">
          
          {{-- messaggio generico di sessione per user loggato --}}
          @if (session('message'))
          <div class="row alert bg_fucsia mt-3 px-3 align-items-center">
            <div class="bg_fucsia decoration-none py-auto">
              {{ session('message') }}
            </div>
          </div>
          @endif
          
          {{-- messaggio di sessione errore revisore only.revisor --}}
          
          @if (session('only.revisor'))
          <div class="row alert bg_fucsia mt-3 px-3 align-items-center">
            <div class="bg_fucsia decoration-none py-auto">
              {{ session('only.revisor') }}
            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
    
    
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-4 p-4">
          {{-- form per la ricerca per testo --}}
          <form action="{{route('search')}}" method="GET">
            <input name="q" type="text" placeholder="Cerca">
            <button class="btn btn-outline-success" type="submit">Cerca</button>
          </form>
          <h1 class="mt-3 mb-2 color_fucsia">Presto, compra e vendi il tuo usato.</h1>
          <h3 class="mt-1 ">Che stai aspettando? Trova il tuo affare!</h3>
        </div>
        <div class="col-12 col-md-8 my-5">
          
          <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
            </div>
            <div class="carousel-inner carousel_custom">
              <div class="carousel-item active ">
                <img src="https://picsum.photos/800/600" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item ">
                <img src="https://picsum.photos/800/601" class="d-block w-100" alt="...">
              </div>
              <div class="carousel-item ">
                <img src="https://picsum.photos/800/602" class="d-block w-100" alt="...">
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
          
        </div>
      </div>
      
      
      {{-- carousel --}}
      
      <div class="container">
        <div class="carousel-container_c">
          <div class="carousel-inner_c">
            <div class="track">
              @foreach ($categories as $category)
              <div class="card-container">
                <div class="card_custom">
                  <div class="img"><div id="cat_bg{{$category->id}}" class="col-6 col-md-2 px-3 m-3 category_box  d-flex flex-column align-items-center justify-content-center text-center category_div">
                    <a class="nav-link  " href="{{route('category.show', compact('category'))}}">
                      <h5 >{{$category->name}}</h5>
                    </a>
                    
                    
                  </div>
                </div>
                
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <div class="nav">
          <button class="prev">
            <i class="material-icons">
              keyboard_arrow_left
            </i>
          </button>
          <button class="next">
            <i class="material-icons">
              keyboard_arrow_right
            </i>
          </button>
        </div>
      </div>
      
    </div>
    
    
    
    
    
    
    
    
    <div class="row p-5">
      <h3 class="mt-1 mb-5" >Ultimi articoli inseriti</h3>
      
      @foreach ($announcements as $announcement)
      <div class="col-12 col-md-6 p-4 ">
        <div class="row">
          <div class="col-6 d-flex align-items-center justify-content-end">
            <img src="https://via.placeholder.com/150 " class="fluid" alt="random pic" >
          </div>
          <div class="col-6 card-body p-5">
            <h5 class="card-title">{{$announcement->title}}</h5>
            <p class="card-text ">{{$announcement->created_at->format('d/m/Y')}}</p>
            <p class="card-text">{{$announcement->category->name}}</p>
            
            <p class="card-text my-3">{{$announcement->price}}</p>
            <a href="{{route('detailAnnouncement', compact('announcement'))}}" class="btn btn_custom">Scopri di più</a>
            
          </div>
          
        </div>
        
      </div>
      
      @endforeach
      
    </div>
    
  </div>
  
  
  <script>
    const prev = document.querySelector('.prev');
    const next = document.querySelector('.next');
    
    const track = document.querySelector('.track');
    
    let carouselWidth = document.querySelector('.carousel-container_c').offsetWidth;
    
    window.addEventListener('resize', () => {
      carouselWidth = document.querySelector('.carousel-container_c').offsetWidth;
    })
    
    let index = 0;
    
    next.addEventListener('click', () => {
      index++;
      prev.classList.add('show');
      track.style.transform = `translateX(-${index * carouselWidth}px)`;
      
      if (track.offsetWidth - (index * carouselWidth) < carouselWidth) {
        next.classList.add('hide');
      }
    })
    
    prev.addEventListener('click', () => {
      index--;
      next.classList.remove('hide');
      if (index === 0) {
        prev.classList.remove('show');
      }
      track.style.transform = `translateX(-${index * carouselWidth}px)`;
    })
  </script>
  
  
  
  
  
  </x-layout>