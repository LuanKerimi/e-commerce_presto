<x-layout>
    <x-slot name="title">{{__('ui.insertNewAd')}}</x-slot>
    
    <div class="container MbClass MtClass">
        <div class="row">
            <div class="col-12 col-md-8 offset-md-2 mt-5 pCustom bgWhite shadow Bradius">
                <h1 class="color_fucsia text-center  my-5">Modifica annuncio</h1>
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif  
                <form method="POST" action="{{route('update', compact('announcement'))}}"> 
                    @csrf
                    @method('put')
                    <div class="my-3">
                        <label for="InputTitle" class="form-label">{{__('ui.title')}}</label>
                        <input type="text" class="form-control" id="InputTitle" name="title" value="{{$announcement->title}}">
                    </div>

                    <div>
                        <label for="InputTitle" class="form-label">{{__('ui.categ')}}</label>
                            <select name="category" class="form-select">
                            <option value="{{$announcement->category->id}}">{{$announcement->category->name}}</option>
                            @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                    </select>
                    </div>
                   

                    <div class="my-3">
                        <label for="InputTitle" class="form-label">{{__('ui.Price')}}</label>
                        <input type="number" class="form-control" id="InputTitle" name="price" value="{{$announcement->price}}">
                    </div>


                    <div class="mb-3">
                        <label for="InputDescription" class="form-label">{{__('ui.desc')}}</label>
                        <textarea class="form-control" name="body" id="InputDescription">{{$announcement->body}}</textarea>
                    
                    </div>

{{--                     <label class="form-label">{{__('ui.insertPic')}}</label>
 --}}

                    {{-- dropzone --}}
                   
{{--                     <div class="dropzone" id="drophere" name="img" ></div>
 --}}
                   {{--  @error('images')
                     <span class="invalid-feedback" role="alert"><strong>{{$message}}</strong> </span>
                     @enderror --}}
                    
                    <button type="submit" class="btn btn_custom my-3">Aggiorna</button>
                    
                </form>

                <div class="row col-12 col-md-12 text-center mt-5">
                     <a href="{{route('home')}}">
                    <button class="btn btn_custom_out my-5">{{__('ui.annulla')}}</button>
                </a>
                </div>
               
                
                
            </div>
        </div>
    </div>
    
    
    


</x-layout>