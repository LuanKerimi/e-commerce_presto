<x-layout>
        <x-slot name="title">{{__('ui.insertNewAd')}}</x-slot>
        
        <div class="container  MtClass MbClass">
            <div class="row">
                <div class="col-12 col-md-8 offset-md-2 mt-5 pCustom bgWhite shadow Bradius">
                    <h1 class="color_fucsia MbClass text-center">{{__('ui.insertNewAd')}}</h1>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif  
                    <form method="POST" action="{{route('store')}}" 
                    enctype="multipart/form-data"> 
                        @csrf
                    {{-- togliere segreto --}}
                        {{-- <h3>{{$uniqueSecret}}</h3> --}}

                        <div class="my-3">
                            <label for="InputTitle" class="form-label">{{__('ui.title')}}</label>
                            <input type="text" class="form-control" id="InputTitle" name="title" value="{{old("title")}}">
                        </div>

                        <div>
                            <label for="InputTitle" class="form-label">{{__('ui.categ')}}</label>
                                <select name="category" class="form-select">
                                <option value=0>{{__('ui.pickCateg')}}</option>
                                @foreach ($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                        </select>
                        </div>
                       

                        <div class="my-3">
                            <label for="InputTitle" class="form-label">{{__('ui.Price')}}</label>
                            <input type="number" class="form-control" id="InputTitle" name="price" value="{{old("price")}}">
                        </div>


                        <div class="mb-3">
                            <label for="InputDescription" class="form-label">{{__('ui.desc')}}</label>
                            <textarea class="form-control" name="body" id="InputDescription">{{old("description")}}</textarea>
                        
                        </div>

                        <label class="form-label">{{__('ui.insertPic')}}</label>



                        <input type="hidden" name="uniqueSecret" value="{{$uniqueSecret}}">

                        
                        {{-- dropzone --}}
                       
                        <div class="dropzone" id="drophere" name="img" ></div>

                        @error('images')
                         <span class="invalid-feedback" role="alert"><strong>{{$message}}</strong> </span>
                         @enderror
                        
                        <button type="submit" class="btn btn_custom my-3">{{__('ui.save')}}</button>
                        
                    </form>

                    <div class="row col-12 col-md-12 text-center mt-5">
                         <a href="{{route('home')}}">
                        <button class="btn btn_custom_out my-5">{{__('ui.annulla')}}</button>
                    </a>
                    </div>
                   
                    
                    
                </div>
            </div>
        </div>
        
        
        
    

</x-layout>