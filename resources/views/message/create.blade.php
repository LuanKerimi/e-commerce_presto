<x-layout>

    {{-- {{dd($sender, $addressee)}} --}}
    <div class="container-fluid my-5 py-5 bg-ligh ">
    <div>
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        @if (session('message')) 
        <div class="alert alertCustom Bradius col-8 offset-2 my-5 "> 
          {{ session('message') }}
        </div>       
        @endif
    </div>
        <div class="row mx-auto justify-content-center wide mt-5">
            <div class="col-6">
                <form action="{{route('messageSendContact', compact('sender', 'addressee'))}}" method="POST" class="mt-5">
                    @csrf
                    <div class="mb-3">
                        <label class="form-label">Scrivi</label>
                        <input type="text" class="form-control" name="message">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Conferma</button>

                </form>
            </div>
            
            
            
        </div>
    </div>
</x-layout>