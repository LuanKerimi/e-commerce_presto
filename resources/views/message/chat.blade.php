<x-layout>

    {{-- <div class="container mt-5">
        <div class="row mt-5">
            <div class="col-12 mt-5">
                 @foreach ($messages_tot as $message)
                
                @if ($message->user_id == $thisUser->id) 
             
                <div class="mb-3 bg-danger">
                    <p class="">{{$message->user->name}} scrive: {{$message->message}}</p>
                   </div>   

                @else

              <div class="mb-3 bg-success">
                   <p class="">{{$message->user->name}} scrive: {{$message->message}}</p>                   
                  </div>
               @endif     
                    
                @endforeach 
               
                <div class="col-6">
                    <form action="{{route('messageSend', compact( 'sender','addressee'))}}" method="POST" class="mt-5">
                        @csrf
                        <div class="mb-3">
                            <label class="form-label">Scrivi</label>
                            <input type="text" class="form-control" name="message">
                        </div>
                        
                        <button type="submit" class="btn btn-primary">Conferma</button>
    
                    </form>
                </div> 


            </div>
        </div>
    </div> --}}


        <div class="container mt-5">
            <div class="row">
                <div class=" text-center col-12 mt-5">
                    @foreach ($messages_tot as $messages)
                        @if ($thisUser == $messages->user)
                        @else                
                            <h2> Chat con : {{$messages->user->name}}</h2>
                            @break
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    
        <div class="container">
            <div class="row g-1">
    
                <div class="col-12 col-md-8 mb-5 offset-md-2">
                    <div class="card p-4 ">
    
                        <div class="card p-4 pb-0 msgArea mb-1">
                            @foreach ($messages_tot as $message)

                                @if ($message->user_id == $thisUser->id) 
                                    <div class="d-inline-flex justify-content-end  pb-3 mt-2 ">
                                        <p class="m-0 pe-2 msgStyleMit bgChatOut col-10 col-md-7 txtGrey p-2"> {{$message->message}}</p>
                                    </div>   

                                @else

                                    <div class="d-inline-flex justify-content-start row pb-3 mt-2 ">
                                    <p class="m-0 pe-2 msgStyleMit bgChatIn txtGrey p-2 col-10 col-md-7">{{$message->message}}</p>                   
                                    </div>
                                @endif     

                            @endforeach
                    
    
                      
                        
                        <form action="{{route('messageSendX', compact( 'sender','addressee'))}}" method="POST" class="mt-5">
                            <div class="form-floating">
                            @csrf
                                <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea2" style="height: 100px" name="message"></textarea>
                                <label for="floatingTextarea2">Scrivi Messaggio</label>
                       
    
                                <div class="d-flex justify-content-end pt-3">
                                    <button type="submit" class="btn btn-primary">Invia</button>
                                </div>                                                                                   
                            </div>
                        </form>
                                        
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    
    </x-layout>
