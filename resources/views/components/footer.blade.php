

  <div class="container-fluid bgWhite pt-4 border-top">
    <footer class="container" >
      <ul class="nav justify-content-center border-bottom pb-3 my-0">
        <li class="nav-item"><a href="" class="nav-link-black px-2 text-muted">Home</a></li>
        <li class="nav-item"><a href="" class="nav-link-black px-2 text-muted">{{__('ui.reviews')}}</a></li>
        <li class="nav-item"><a href="#" class="nav-link-black px-2 text-muted">{{__('ui.chiSiamo')}}</a></li>
        <li class="nav-item"><a href="#" class="nav-link-black px-2 text-muted">FAQs</a></li>
        <li class="nav-item"><a href="#" class="nav-link-black px-2 text-muted">About</a></li>
      </ul>

      <div class="col-12 d-flex align-items-center justify-content-center">
        <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
          <img src="/img/Loghi-Presto/PNG/faviconSito.png" alt="" height="20px">
        </a>
        <p class="text-center mt-4">© 2021 Presto | Made by "I Croccanti"</p>
      </div>
  

      
    </footer>
  </div>