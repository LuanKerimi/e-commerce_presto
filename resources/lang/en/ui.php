
<?php
return [

    'contatta'=> 'Contact',
    'mexHome' => 'Messages',
    'homeH1' => 'Fast, buy and sell your used.',
    'homeHeaderH3' => "What are you waiting for?",
    'homeHeaderh32' => 'Find your deal and promote recycling!',

    'homeHeaderH6' => "SUPPORT THE ENVIRONMENT",
    'homeBtnCerca' => 'Search',

    'homeCategorieH3' => 'EXPLORE CATEGORIES',
    'homeDescCategorie' => "it's a long story It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using ",


    'homePlus' => 'The best deal is always the safe one',

    'homePagaH2' => 'PAY SAFELY',
    'homePagaDesc' => 'Use Pay pal for online payments',
    'homePagaDesc2' => 'Credit or rechargeable cards allow you to make payments in real time',
    'homePagaDesc3' => 'You can also opt for a bank transfer',


    'homeVendiH2' => 'FREE SELL',
    'homeVendiDesc' => "Post your ad for free in over 10 categories",
    'homeVendiDesc2' => "Sale at no additional cost, even when you sell, no commission",
    'homeVendiDesc3' => "Millions of interested users",

    'homeUltimiAnnunciH3' => 'LAST ANNOUNCEMENTS PLACED',

    'homeUltimiAnnunciDesc' => ' description It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using',

    'btnScopri' => 'Find out more',

    'HomeH2NewsL' => 'Every day a new deal!',
    'HomeNewsLdesc' => 'Enter your email to stay up to date.',

    'btnHomeSubmit' => 'Sign Up',
    /*revisorHome*/

    'MyAnn' => 'YOUR ANNOUNCEMENTS',

    'RevHh3' => 'ANNOUNCEMENTS TO REVIEW',
    'RevHnoAds' => 'There is no announcements to review',
    'RevHp' => 'Back to home',
    'RevHBtn' => 'Home',
    'Price' => 'Price ',
    'Rifiuta' => 'Reject',
    'Accetta' => 'Accept',
    'RecApp' => 'Recover approved',
    'RecRif' => 'Recover rejected',
    'Recupera' => 'Recover',

    //components

    'btnInsAnn' => 'Insert +',
    'chiSiamo' => 'About us',
    'reviews' => 'Reviews',
    'register' => 'Register',
    'hello' => 'Hello',
    'yourAds' => 'Your announcements',
    'revisorHome' => 'Revisor Home',
    'becomeRev' => 'Become a revisor',

    //showCategory
    'searchRes' => 'Results for: ',
    //dashboard
    'articoliDi' => 'Announcements of: ',
    //login
    'bentornato' => 'Welcome back!',
    'insertEmail' => 'Insert your e-mail',
    'insertPass' => 'Insert your password',
    'accedi' => 'Sign in',
    'notRegister' => 'Not registered yet?',
    'cliccaqui' => 'Click here',
    //register
    'insertName' => 'Insert your name',
    'confPass' => 'Confirm password',
    'registrato' => 'Already registered?',
    //RevReq
    'telluswhy' => ', tell us briefly why you want to become a revisor.',
    'insertMex' => 'Insert your message',
    'submit' => 'Submit',
    //create
    'insertNewAd' => 'Insert a new announcement',
    'title' => 'Title',
    'categ' => 'Category',
    'pickCateg' => 'Pick the category',
    'desc' => 'Give a short description',
    'insertPic' => 'Insert pictures',
    'save' => 'Save',
    'annulla' => 'Cancel',
    'infovenditore' => 'Seller info',
    'venditoredal' => 'Seller since: '



];
