<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use Laravel\Sail\Console\PublishCommand;
use App\Http\Controllers\PublicController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\RevisorController;
use App\Http\Controllers\AnnouncementController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PublicController::class, 'index'])->name('home');
Route::get('/new/announcement', [AnnouncementController::class, 'create'])->name('create');
Route::post('/store/announcement/', [AnnouncementController::class, 'store'])->name('store');

Route::get('/edit/{announcement}', [AnnouncementController::class, 'edit'])->name('edit');


Route::put('/update/{announcement}', [AnnouncementController::class, 'update'])->name('update');

Route::delete('/delete/{announcement}', [AnnouncementController::class, 'destroy'])->name('delete');

Route::get('/announcement/images', [AnnouncementController::class, 'getImages'])->name('announcement.images');

Route::get('category/show/{category}', [PublicController::class, 'showCategory'])->name('category_show');

Route::get('announcement/detail/{announcement}', [AnnouncementController::class, 'show'])->name('detailAnnouncement');

Route::get('/test', [PublicController::class, 'test'])->name('test');

/* Rotte per l'utente revisore */
Route::get('/revisor/home', [RevisorController::class, 'index'])->name('revisorHome');
Route::post('/revisor/announcement/{id}/accept', [RevisorController::class, 'accept'])->name('revisorAccept');
Route::post('/revisor/announcement/{id}/reject', [RevisorController::class, 'reject'])->name('revisorReject');

Route::get('/search', [PublicController::class, 'search'])->name('search');

Route::get('/revisor/request', [AuthController::class, 'revisorRequest'])->name('revisorRequest');
Route::post('/revisorSubmit', [AuthController::class, 'revisorSubmit'])->name('revisorSubmit');

Route::post('/newsletterSubmit', [PublicController::class, 'newsletterSubmit'])->name('newsletterSubmit');

Route::post('/revisor/announcement/{id}/undo', [RevisorController::class, 'undo'])->name('revisorUndo');

Route::get('/dashboard', [AuthController::class, 'index'])->name('userAnnouncements');

Route::post('/announcement/images/upload', [AnnouncementController::class, 'uploadImages'])->name('uploadImages');

Route::delete('/announcement/images/remove', [AnnouncementController::class, 'removeImages'])->name('removeImages');

Route::post('/locale/{locale}', [PublicController::class, 'locale'])->name('locale');



Route::get('/message/create/{user_id}', [MessageController::class, 'create'])->name('messageCreate');
Route::post('/chat/{addressee}/', [MessageController::class, 'store'])->name('messageSendX');
Route::post('/message/{addressee}', [MessageController::class, 'store2'])->name('messageSendContact');
// Route::post('/message/send/{addressee}', [MessageController::class, 'store'])->name('messageSend');
Route::get('/message/index', [MessageController::class, 'index'])->name('messageIndex');
Route::get('/message/{user_id}', [MessageController::class, 'chatDetail'])->name('messageDetail');


Route::get('msglist', [AuthController::class, 'msglist'])->name('msglist');

Route::get('msgdetail', [AuthController::class, 'msgDetail'])->name('msgDetail');



